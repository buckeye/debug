# debug

A BuckleScript wrapper for the `debug` library.

## Examples

You can turn the example file:

```sh
DEBUG=my:namespace node ./examples/Basic.bs.js
```

Your output should look something like this:

```sh
  my:namespace something +0ms
  my:namespace thing +2ms
  my:namespace thing +0ms
  my:namespace logfMany, uses formatting, answer = 42 +0ms
  my:namespace tapfMany formats and passes through, answer = 42 +0ms
  my:namespace [ 'tapfMany', [ 42, 0 ] ] +0ms
  my:namespace logf formats one value +1ms
  my:namespace tapf formats and passes through +0ms
  my:namespace tapf +0ms
  my:namespace logf2 formats 2 values, answer = 42 +0ms
  my:namespace tapf2 formats 2 values and passes through, answer = 42 +0ms
  my:namespace [ 'tapf2', 42 ] +1ms
  my:namespace You can manually turn off your logger +0ms
  my:namespace This should print +0ms
```

### Usage

```reasonml
let debug = Debug.make("my:namespace");

Debug.log(debug, "something");

Debug.tap(debug, "thing") |> Debug.log(debug);

Debug.logfMany(
  debug,
  "%s, uses formatting, answer = %d",
  [Debug.Arg.t("logfMany"), Debug.Arg.t(42)],
);

Debug.tapfMany(
  debug,
  "%s formats and passes through, answer = %d",
  [Debug.Arg.t("tapfMany"), Debug.Arg.t(42)],
)
|> Debug.log(debug);

Debug.logf(debug, "%s formats one value", "logf");

Debug.tapf(debug, "%s formats and passes through", "tapf")
|> Debug.log(debug);

Debug.logf2(debug, "%s formats 2 values, answer = %d", ("logf2", 42));

Debug.tapf2(
  debug,
  "%s formats 2 values and passes through, answer = %d",
  ("tapf2", 42),
)
|> Debug.log(debug);

Debug.log(debug, "You can manually turn off your logger");
Debug.enabledSet(debug, false);
Debug.log(debug, "This should not print");
Debug.enabledSet(debug, true);
Debug.log(debug, "This should print");
```
