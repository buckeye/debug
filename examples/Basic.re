let debug = Debug.make("my:namespace");

Debug.log(debug, "something");

Debug.tap(debug, "thing") |> Debug.log(debug);

Debug.logfMany(
  debug,
  "%s, uses formatting, answer = %d",
  [Debug.Arg.t("logfMany"), Debug.Arg.t(42)],
);

Debug.tapfMany(
  debug,
  "%s formats and passes through, answer = %d",
  [Debug.Arg.t("tapfMany"), Debug.Arg.t(42)],
)
|> Debug.log(debug);

Debug.logf(debug, "%s formats one value", "logf");

Debug.tapf(debug, "%s formats and passes through", "tapf")
|> Debug.log(debug);

Debug.logf2(debug, "%s formats 2 values, answer = %d", ("logf2", 42));

Debug.tapf2(
  debug,
  "%s formats 2 values and passes through, answer = %d",
  ("tapf2", 42),
)
|> Debug.log(debug);

Debug.log(debug, "You can manually turn off your logger");
Debug.enabledSet(debug, false);
Debug.log(debug, "This should not print");
Debug.enabledSet(debug, true);
Debug.log(debug, "This should print");
