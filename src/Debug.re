module Arg = {
  type t;

  external t: 'a => t = "%identity";
};

[@bs.deriving abstract]
type t = {
  mutable enabled: bool,
  mutable log: array(Arg.t) => unit,
};

[@bs.module] external make: string => t = "debug";

module Native = {
  let log: (. t, array(Arg.t)) => unit = [%raw
    {|
  function(debug, args) {
    debug(...args);
  }
|}
  ];
};

let log = (t, arg) => Native.log(. t, [|arg->Arg.t|]);

let tap = (t, arg) => log(t, arg) |> (_ => arg);

let logfMany = (t, string, args) =>
  Native.log(. t, [Arg.t(string), ...args] |> Belt.List.toArray);

let tapfMany = (t, string, args) => logfMany(t, string, args) |> (_ => args);

let logf = (t, string, a) => logfMany(t, string, [a->Arg.t]);

let logf2 = (t, string, (a, b)) =>
  logfMany(t, string, [a->Arg.t, b->Arg.t]);

let tapf = (t, string, a) => logf(t, string, a) |> (_ => a);

let tapf2 = (t, string, (a, b)) =>
  logf2(t, string, (a, b)) |> (_ => (a, b));
